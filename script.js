let buttonMenu$$ = document.getElementById('buttonMegaMenu');
const headerNav$$ = document.querySelector('.header-nav');

const changeButtonMenu = () => {
    const collapsed = buttonMenu$$.getAttribute('aria-expanded');
    if (collapsed === 'false') {
        buttonMenu$$.setAttribute('aria-expanded', true);
    } else {
        buttonMenu$$.setAttribute('aria-expanded', false);
    }
    return collapsed;
}

const collapseNavBar = (collapsed) => {
    if (collapsed === 'false') {
        headerNav$$.classList.add('collapse');
    } else {
        headerNav$$.classList.remove('collapse');
    }
}

headerNav$$.addEventListener('click', function(e) {
    const collapsed = changeButtonMenu();
    collapseNavBar(collapsed);
});